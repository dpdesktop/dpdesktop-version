<?php

header('Content-type: text/xml');

$dom = new DOMDocument('1.0', 'utf-8');

$root = $dom->appendChild( $dom->createElement("root") );

$version = $dom->createElement("version");
$version->setAttribute("value", "0.70");

$root->appendChild($version);

/*
$url = $dom->createElement("url");
$url->setAttribute("value", "http://www.sf.net/projects/dpdesktop");

$root->appendChild($url); 
*/
echo $dom->saveXML();

?>
